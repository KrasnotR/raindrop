#include <string.h>

char* strcpy(char* restrict dst, const char* restrict src) {
    const size_t length = strlen(src);
    // The strcpy() function copy the string src to dst
    memcpy(dst, src, length+1);
    return dst;
}