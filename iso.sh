#!/bin/sh
set -e
. ./build.sh

mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub

cp sysroot/boot/raindrop.kernel isodir/boot/raindrop.kernel
cat > isodir/boot/grub/grub.cfg << EOF
set 
set timeout=1
set default=0

set menu_color_normal=white/black
set menu_color_highlight=white/cyan

menuentry "Raindrop" {
	multiboot /boot/raindrop.kernel
}
EOF
grub-mkrescue -o raindrop.iso isodir
