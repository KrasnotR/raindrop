#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H


#include <stddef.h>
#include <stdint.h>

struct vec2 {
    uint8_t x;
    uint8_t y;
};

void terminal_initialize(void);
void terminal_putchar(char c);
void terminal_write(const char* data, size_t size);
void terminal_writestring(const char* data);
void terminal_enable_cursor(uint8_t cursor_start, uint8_t cursor_end);
void terminal_disable_cursor(void);
void terminal_update_cursor(int x, int y);
uint16_t terminal_get_cursor_pos(void);
struct vec2 terminal_get_pos(void);


#endif