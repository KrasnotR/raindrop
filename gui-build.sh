#!/bin/bash

if ! echo "$PATH" | grep -q "$HOME/opt/cross/bin:"; then
    export PATH="$HOME/opt/cross/bin:$PATH"
fi
mkdir -p isodir/boot/grub

if [ "$1" == "start" ] || [ "$1" == "s" ]; then
{
	cd src
	printf "XXX\n0\nBuilding bootloader\nXXX\n"
	i686-elf-as boot.s -o boot.o > /dev/null 2>&1
	printf "XXX\n20\nBuilding kernel\nXXX\n"
	i686-elf-gcc -c kernel.c -o kernel.o -std=gnu99 -ffreestanding -O3 -Wall -Wextra -fno-exceptions -fno-rtti > /dev/null 2>&1
	printf "XXX\n40\nLinking kernel\nXXX\n"
	i686-elf-gcc -T linker.ld -o Raindrop.bin -ffreestanding -O3 -nostdlib boot.o kernel.o -lgcc > /dev/null 2>&1

	cd ..
	cp src/Raindrop.bin isodir/boot/Raindrop.bin > /dev/null 2>&1
	rm src/boot.o src/kernel.o src/Raindrop.bin > /dev/null 2>&1
	cp src/grub.cfg isodir/boot/grub/grub.cfg > /dev/null 2>&1

	printf "XXX\n60\nBuilding ISO\nXXX\n"
	grub-mkrescue -o Raindrop.iso isodir > /dev/null 2>&1
	printf "XXX\n100\nRunning the ISO with QEMU\nXXX\n"
} | whiptail --title "Building Raindrop" --gauge "Building Raindrop" 6 60 0
	qemu-system-i386 -cdrom Raindrop.iso
else
{
	cd src
	printf "XXX\n0\nBuilding bootloader\nXXX\n"
	i686-elf-as boot.s -o boot.o > /dev/null 2>&1
	printf "XXX\n20\nBuilding kernel\nXXX\n"
	i686-elf-gcc -c kernel.c -o kernel.o -std=gnu99 -ffreestanding -O3 -Wall -Wextra -fno-exceptions -fno-rtti > /dev/null 2>&1
	printf "XXX\n40\nLinking kernel\nXXX\n"
	i686-elf-gcc -T linker.ld -o Raindrop.bin -ffreestanding -O3 -nostdlib boot.o kernel.o -lgcc > /dev/null 2>&1

	cd ..
	cp src/Raindrop.bin isodir/boot/Raindrop.bin > /dev/null 2>&1
	rm src/boot.o src/kernel.o src/Raindrop.bin > /dev/null 2>&1
	cp src/grub.cfg isodir/boot/grub/grub.cfg > /dev/null 2>&1

	printf "XXX\n60\nBuilding ISO\nXXX\n"
	grub-mkrescue -o Raindrop.iso isodir > /dev/null 2>&1
	printf "XXX\n100\nDone\nXXX\n"
} | whiptail --title "Building Raindrop" --gauge "Building Raindrop" 6 60 0
fi
